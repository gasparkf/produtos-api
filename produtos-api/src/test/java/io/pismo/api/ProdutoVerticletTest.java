package io.pismo.api;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.pismo.base.entidade.Produto;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ProdutoVerticletTest {

	private Vertx vertx;
	private Integer port;


	@BeforeClass
	public static void initialize() throws IOException {
		
	}

	@AfterClass
	public static void shutdown() {
		
	}

	/**
	 * Before executing our test, let's deploy our verticle.
	 * <p/>
	 * This method instantiates a new Vertx and deploy the verticle. Then, it waits in the verticle has successfully
	 * completed its start sequence (thanks to `context.asyncAssertSuccess`).
	 *
	 * @param context the test context.
	 */
	@Before
	public void setUp(TestContext context) throws IOException {
		vertx = Vertx.vertx();

		// Let's configure the verticle to listen on the 'test' port (randomly picked).
		// We create deployment options and set the _configuration_ json object:
		port = 8081;
		
		// We pass the options as the second parameter of the deployVerticle method.
		vertx.deployVerticle(ProdutoVerticle.class.getName());
	}

	/**
	 * This method, called after our test, just cleanup everything by closing the vert.x instance
	 *
	 * @param context the test context
	 */
	@After
	public void tearDown(TestContext context) {
		vertx.close(context.asyncAssertSuccess());
	}

	/**
	 * Let's ensure that our application behaves correctly.
	 *
	 * @param context the test context
	 */
	@Test
	public void testeMensagemInicial(TestContext context) {
		final Async async = context.async();

		vertx.createHttpClient().getNow(port, "localhost", "/", response -> {
			response.handler(body -> {
				context.assertTrue(body.toString().contains("produtos-api"));
				async.complete();
			});
		});
	}

	@Test
	public void testeGravarProduto(TestContext context) {
		Async async = context.async();
		final String json = Json.encodePrettily(new Produto("Iphone 7", "Smartphone", 3200.00));
		vertx.createHttpClient().post(port, "localhost", "/api/produtos")
		.putHeader("content-type", "application/json")
		.putHeader("content-length", Integer.toString(json.length()))
		.handler(response -> {
			context.assertEquals(response.statusCode(), 201);
			context.assertTrue(response.headers().get("content-type").contains("application/json"));
			response.bodyHandler(body -> {
				final Produto produto = Json.decodeValue(body.toString(), Produto.class);
				context.assertEquals(produto.getDescricao(), "Iphone 7");
				context.assertEquals(produto.getCategoria(), "Smartphone");
				context.assertEquals(produto.getValor(), 3200.00);
				context.assertNotNull(produto.getId());
				async.complete();
			});
		})
		.write(json)
		.end();
	}
	
	@Test
	public void testeBuscarPorCategoria(TestContext context) {
		Async async = context.async();
		vertx.createHttpClient().get(port, "localhost", "/api/produtos/Teste")
		.putHeader("content-type", "application/json")
		.handler(response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get("content-type").contains("application/json"));
			response.bodyHandler(body -> {
				final Produto produto = Json.decodeValue(body.toString(), Produto.class);
				context.assertEquals(produto.getDescricao(), "Iphone 6S");
				context.assertEquals(produto.getCategoria(), "Teste");
				context.assertEquals(produto.getValor(), 5.0);
				context.assertNotNull(produto.getId());
				async.complete();
			});
		})
		.end();
	}
}