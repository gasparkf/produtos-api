package io.pismo.api;

import io.pismo.base.dao.DAO;
import io.pismo.base.entidade.Produto;
import io.pismo.util.RespostaUtil;
import io.pismo.util.Runner;
import io.pismo.util.SegurancaUtil;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ProdutoVerticle extends AbstractVerticle {
	
	
	private JDBCClient client;

	public static void main(String[] args) {
		Runner.run(ProdutoVerticle.class);
	}

	@Override
	public void start() throws Exception {
		DAO.iniciar(vertx, "jdbc:postgresql://localhost:5432/pismoDB", "postgres", "org.postgresql.Driver", "postgres", 15);
		
		Router router = Router.router(vertx);
		
		router.get("/api/produtos").handler(this::buscarTodos);
		router.get("/api/produtos/:categoria").handler(this::buscarTodosPorCategoria);
		router.route("/api/produtos*").handler(BodyHandler.create());
		router.post("/api/produtos").handler(this::gravar);
		
		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response
			.putHeader("content-type", "text/html")
			.end("<h1>API para recuperar e gravar produtos - produtos-api</h1>");
		});

		vertx
		.createHttpServer()
		.requestHandler(router::accept)
		.listen(8081);
	}
	
	@Override
	public void stop() throws Exception {
		if(client != null){
			client.close();
		}
		super.stop();
	}
	
	private void buscarTodos(RoutingContext routingContext) {
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
		
				String sql= "SELECT * FROM produto";
				DAO.buscarTodos(sql, rs -> {
					if (rs.size() > 0) {
						RespostaUtil.respostaErro(200, routingContext, rs.toString());
					}else{
						RespostaUtil.respostaErro(400, routingContext);
					}
				});
			}
		});
	}
	
	private void gravar(RoutingContext routingContext) {
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
				final Produto produto = Json.decodeValue(routingContext.getBodyAsString(), Produto.class);
				
				String sql = "INSERT INTO produto (descricao, categoria, valor) VALUES (?,?,?)";
				JsonArray param = new JsonArray();
				param.add(produto.getDescricao());
				param.add(produto.getCategoria());
				param.add(produto.getValor());
				
				DAO.gravar(sql.toString(), param, rs -> {
					if(rs != null){
						produto.setId(rs.longValue());
						RespostaUtil.respostaErroEP(201, routingContext, produto);
					}else{
						RespostaUtil.respostaErro(400, routingContext);
					}
						
				});
			}
		});
		
	}
	
	private void buscarTodosPorCategoria(RoutingContext routingContext){
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
				String filtro = routingContext.request().getParam("categoria");
				String sql = "SELECT * FROM produto WHERE categoria = ?";
						
				if (filtro == null) {
					routingContext.response().setStatusCode(400).end();
				} else {
					DAO.buscarTodos(sql, new JsonArray().add(filtro), rs ->{
						if(rs.size() > 0){
							RespostaUtil.respostaErro(200, routingContext, rs.toString());
							//rs.toString().replaceAll("\\[|\\]", "")
						}else{
							RespostaUtil.respostaErro(400, routingContext);
						}
					});
				}
			}
		});
	}

}
